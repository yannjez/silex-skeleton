<?php
/**
 * Created by PhpStorm.
 * User: yann
 * Date: 13/05/2015
 * Time: 8:10
 */


namespace Service;

use Symfony\Component\Config\Definition\Exception\Exception;

class testService {

    /** @var  \Silex\Application */
    protected  $app ;
    /** @var   \Doctrine\DBAL\Connection */
    protected $connexion;


    public function  __construct($app){
        $this->app = $app;
        $this->connexion = $app['dbs']['base'];
    }



    public function getList(){
        $sql = 'select * from url';
        $result =$this->connexion->fetchAll($sql);
        return $result ;
    }


    public function insertRow($data){
        $countAffected = $this->connexion->insert('url',$data);
        $newId = $this->connexion->lastInsertId();
    }

    public function updateRow($id,$data){
        $countAffected = $this->connexion->update('url',$data,array('id'=>$id));
    }


    public function deleteRow($id){
        $countAffected = $this->connexion->delete('url',array('id'=>$id));
    }



}