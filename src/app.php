<?php

/** @var \Composer\Autoload\ClassLoader $appLoader */



use Silex\Application;
use Silex\Provider\TwigServiceProvider;
use Silex\Provider\UrlGeneratorServiceProvider;
use Silex\Provider\ValidatorServiceProvider;
use Silex\Provider\ServiceControllerServiceProvider;



$app = new Application();
$app->register(new UrlGeneratorServiceProvider());
$app->register(new ValidatorServiceProvider());
$app->register(new ServiceControllerServiceProvider());
$app->register(new Silex\Provider\SessionServiceProvider());
$app->register(new Silex\Provider\DoctrineServiceProvider());


$app->register(new TwigServiceProvider());
$app['twig'] = $app->share($app->extend('twig', function($twig)  use ($app ){
    $twig->getExtension('core')->setTimezone('Europe/Paris');
    $twig->getExtension('core')->setDateFormat('d/m/Y', '%d jour');
    $twig->addFunction(new \Twig_SimpleFunction('asset', function ($asset) use ($app) {
        return $app['request']->getBasePath().'/'.$asset;
    }));
    return $twig;
}));





return $app;
