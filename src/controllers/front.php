<?php

$ctrl = $app['controllers_factory'];

$ctrl->get('/', function () use ($app) {
    return $app['twig']->render('front/index.twig', array());
})->bind('homepage');


return $ctrl;