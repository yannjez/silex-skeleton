<?php
/**
 * Created by PhpStorm.
 * User: marc
 * Date: 20/11/2014
 * Time: 09:34
 */

namespace Controllers;

use Silex\Application;
use Silex\ControllerProviderInterface;
use Symfony\Component\HttpFoundation\Request;

class admin implements ControllerProviderInterface   {

    public function connect(Application $app) {
        /** @var \Silex\ControllerCollection $ctrl */
        $ctrl = $app['controllers_factory'];
        $ctrl->get('/', 'Controllers\admin::loginGet')->bind('admin--login');
        $ctrl->get('/accueil', 'Controllers\admin::accueilGet')->bind('admin--accueil');
        $ctrl->get('/logout', 'Controllers\admin::logoutGet')->bind('admin--logout');
        $ctrl->post('/', 'Controllers\admin::loginPost')->bind('admin--login-post');
        return $ctrl;
    }

    public function loginGet(Application $app) {
        return 'login page';
    }

    public function logoutGet(Application $app) {
        return $app->redirect($app['url_generator']->generate('admin--accueil'));
    }

    public function accueilGet(Application $app) {
        return 'admin welcome page';
    }


    public function loginPost(Application $app, Request $request) {
        $login = $request->get("login");
        $password = $request->get("password");
        $logged = ($password ==='test' &&  $login==='test' );
        if ($logged) {
            $app['session']->set('isLoggedAsAdmin',true);
           return ' logged';
        } else {
            return ' login failed';
        }
    }


}